(ns highspot-takehome-exercise.core-test
  (:require [clojure.test :refer :all]
            [highspot-takehome-exercise.core :refer :all]))

(deftest test-core

  (testing "remove-playlists"
    (is (=
         [{:id "1"}]
         (remove-playlists ["2" "3"] [{:id "1"} {:id "2"} {:id "3"}]))
        "happy path"))

  (testing "add-songs-to-playlists"
    (is (=
         [{:id "1" :song_ids ["1" "2"]} {:id "2" :song_ids ["3" "4"]}]
         (add-songs-to-playlists
          {"1" ["2"]}
          [{:id "1" :song_ids ["1"]} {:id "2" :song_ids ["3" "4"]}]))
        "happy path"))

  (testing "add-songs-ops-to-domain"
    (is (=
         {"1" ["1" "2"] "2" ["3" "4"]}
         (add-songs-ops-to-domain [{:playlist_id "1" :song_ids ["1" "2"]}
                                   {:playlist_id "2" :song_ids ["3" "4"]}]))
        "happy path"))

  (testing "add-songs-to-playlist"
    (is (=
         {:id "1" :song_ids ["1" "2"]}
         (add-songs-to-playlist {:id "1" :song_ids ["1"]} ["2"]))
        "happy path")))
