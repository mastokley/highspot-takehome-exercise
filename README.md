# highspot-takehome-exercise

## Building the project

Creates a single jar file at `target/uberjar`.

Requires [leiningen](https://leiningen.org/).

    $ lein uberjar
    
## Dependencies

The application requires java to be run.

There's a single dependency on [Cheshire](https://github.com/dakrone/cheshire) to parse and encode json. This is required for development, not usage.

## Usage

### Constructing the changes file

The changes file should contain a top level object with any of three top level keys: `add_playlists`, `remove_playlists`, and `add_songs_to_playlists`.

`add_playlists` should be an array of playlist objects in the same shape as `playlists` in the input file, like this:

    {
      "id":"4",
      "user_id":"1",
      "song_ids":[
        "1",
        "2"
      ]
    }

`remove_playlists` should be an array of playlist ids.

`add_songs_to_playlists` should be an array of objects with `playlist_id` and `song_ids` attributes, like this:

    {
      "playlist_id":"1",
      "song_ids":[
        "1",
        "2"
      ]
    }

See an example changes file at `resources/sample-changes.json`.

### Running the application

Here's how to run the application at the command line:

    $ java -jar target/uberjar/highspot-takehome-exercise-0.1.0-SNAPSHOT-standalone.jar input changes output
    
Or uncompiled, with leiningen:

    $ lein run input changes output
    
Or in the repl:

    highspot-takehome-exercise.core> (-main "input.json" "changes.json" "output.json")

## Testing

Here's how to run the unit tests:

Requires [leiningen](https://leiningen.org/).

    $ lein test

## Future work - handling large files

The application eagerly reads files into memory and eagerly parses them. You could instead read and write the files as a stream and lazily parse/encode that stream. Most or all of the existing functionality is designed to be future proof and compatible with lazy data structures.
