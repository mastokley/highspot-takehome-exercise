(ns highspot-takehome-exercise.validations
  (:require [clojure.set]))

;; might be cool to accumulate errors all in one go

(defn unknown-user-ids
  [add-playlist-ops known-users]
  (let [incoming-user-ids (set (map :user_id add-playlist-ops))
        known-user-ids (set (map :id known-users))]
    (clojure.set/difference incoming-user-ids known-user-ids)))

(defn zero-song-playlist-ids
  [add-playlist-ops]
  ;; `vec` to force realization, required for logging
  (vec (filter #(empty? (:song_ids % [])) add-playlist-ops)))

(defn duplicate-playlist-ids
  [add-playlist-ops known-playlists]
  (let [known-playlist-ids (set (map :id known-playlists))
        incoming-playlist-ids (set (map :id add-playlist-ops))]
    (clojure.set/intersection known-playlist-ids incoming-playlist-ids)))

(defn unknown-song-ids-in-add-playlist-ops
  [add-playlist-ops known-songs]
  (let [known-song-ids (set (map :id known-songs))
        incoming-song-ids (set (flatten (map :song_ids add-playlist-ops)))]
    (clojure.set/difference incoming-song-ids known-song-ids)))

(defn validate-add-playlist-ops
  "throw if we detect an unknown user
   throw if new playlist has no songs
   throw if new playlist would duplicate known playlist ids
   throw if new playlist has any unknown songs"
  [ops known-entities]
  (let [{known-users :users
         known-playlists :playlists
         known-songs :songs} known-entities
        zero-song-playlists (zero-song-playlist-ids ops)
        duplicate-playlists (duplicate-playlist-ids ops known-playlists)
        unknown-songs (unknown-song-ids-in-add-playlist-ops ops known-songs)
        unknown-users (unknown-user-ids ops known-users)]

    (when (seq zero-song-playlists)
      (throw (ex-info
              "zero song playlists found in changes/add_playlists"
              {:zero-song-playlists zero-song-playlists
               :ops ops})))

    (when (seq duplicate-playlists)
      (throw (ex-info
              "already-existing playlist ids found in changes/add_playlists"
              {:duplicate-playlists duplicate-playlists
               :ops ops
               :known-playlists known-playlists})))

    (when (seq unknown-songs)
      (throw (ex-info
              "unknown song ids found in changes/add_playlists "
              {:unknown-songs unknown-songs
               :ops ops
               :known-songs known-songs})))

    (when (seq unknown-users)
      (throw (ex-info
              "unknown user ids in changes/add_playlists "
              {:unknown-users unknown-users
               :ops ops
               :known-users known-users})))

    ops))

(defn unknown-playlist-ids
  [add-songs-ops known-playlists]
  (let [known-playlist-ids (set (map :id known-playlists))
        incoming-playlist-ids (set (keys add-songs-ops))]
    (clojure.set/difference incoming-playlist-ids known-playlist-ids)))

(defn unknown-song-ids-in-add-songs-ops
  [add-songs-ops known-songs]
  (let [known-song-ids (set (map :id known-songs))
        incoming-song-ids (set (flatten (vals add-songs-ops)))]
    (clojure.set/difference incoming-song-ids known-song-ids)))

(defn validate-add-songs-ops
  "throw if we detect either unknown song ids or unknown playlist ids in
   the `ops`"
  [ops known-entities]
  (let [{known-songs :songs known-playlists :playlists} known-entities
        unknown-songs (unknown-song-ids-in-add-songs-ops ops known-songs)
        unknown-playlists (unknown-playlist-ids ops known-playlists)]

    (when (seq unknown-playlists)
      (throw (ex-info
              "unknown playlist ids in changes/add_songs_to_playlists "
              {:unknown-playlists unknown-playlists
               :ops ops
               :known-playlists known-playlists})))

    (when (seq unknown-songs)
      (throw (ex-info
              "unknown song ids in changes/add_songs_to_playlists "
              {:unknown-songs unknown-songs
               :ops ops
               :known-songs known-songs})))

    ops))
