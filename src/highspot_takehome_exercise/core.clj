(ns highspot-takehome-exercise.core
  (:require [highspot-takehome-exercise.validations :as v]
            [highspot-takehome-exercise.io :as io]
            [clojure.spec.alpha :as s])
  (:gen-class))

(s/check-asserts true)

(s/def ::song_id string?)
(s/def ::song_ids (s/coll-of ::song_id :kind vector))
(s/def ::contains-song-ids (s/keys :req-un [::song_ids]))

(defn add-songs-to-playlist
  "append `song-ids`, in order, to `playlist`'s `song_ids`"
  ;; NOTE returns song_ids as lazySeq due to `concat`
  [playlist song-ids]
  {:pre [(s/assert ::song_ids song-ids)
         (s/assert ::contains-song-ids playlist)]
   :post [(s/assert ::contains-song-ids %)]}
  (update playlist :song_ids concat song-ids))

(s/def ::playlist_id string?)
(s/def ::add-songs-op (s/keys :req-un [::playlist_id ::song_ids]))
(s/def ::add-songs-ops (s/coll-of ::add-songs-op))

(defn add-songs-ops-to-domain
  "convert sequence of ops into map keyed on playlist id"
  [ops]
  {:pre [(s/assert ::add-songs-ops ops)]}
  (let [add-to-map (fn [map cur] (assoc
                                  map
                                  (:playlist_id cur)
                                  (:song_ids cur)))]
    (reduce add-to-map {} ops)))

(s/def ::id string?)
(s/def ::contains-id (s/keys :req-un [::id]))
(s/def ::playlists-with-ids (s/coll-of ::contains-id))

(defn add-songs-to-playlists
  ;; TODO consider validating that original `playlists` has all distinct playlist-ids
  [ops playlists]
  {:pre [(s/assert ::playlists-with-ids playlists)]
   :post [(s/assert ::playlists-with-ids %)]}
  (map
   (fn [playlist]
     (if-let [songs-to-add (get ops (:id playlist))]
       (add-songs-to-playlist playlist songs-to-add)
       playlist))
   playlists))

(defn remove-playlists
  "remove any playlist whose id is found in `ids-to-remove`"
  [ids-to-remove playlists]
  {:pre [(s/assert ::playlists-with-ids playlists)]
   :post [(s/assert ::playlists-with-ids %)]}
  (let [ids (into #{} ids-to-remove)
        in-ids? (partial contains? ids)]
    (remove (comp in-ids? :id) playlists)))

(defn update-and-write
  [output-filename original-entities updated-playlists]
  (println "writing...")
  (io/write-as-json
   output-filename
   (assoc original-entities :playlists updated-playlists))
  (println "wrote" output-filename))

;; TODO consider more graceful failure of wrong number of args received
(defn -main
  [input-filename changes-filename output-filename]
  (let [known-entities (io/read-entire-json-file-into-memory input-filename)
        {known-playlists :playlists} known-entities
        changes (io/read-entire-json-file-into-memory changes-filename)
        {remove-playlist-ops :remove_playlists
         add-playlist-ops :add_playlists
         raw-add-songs-ops :add_songs_to_playlists :or {remove-playlists-ops []
                                                        add-playlist-ops []
                                                        raw-add-songs-ops []}} changes
        domain-add-songs-ops (add-songs-ops-to-domain raw-add-songs-ops)
        validated-add-songs-ops (v/validate-add-songs-ops domain-add-songs-ops known-entities)
        validated-add-playlist-ops (v/validate-add-playlist-ops add-playlist-ops known-entities)]

    (->> known-playlists
         (remove-playlists remove-playlist-ops)
         (add-songs-to-playlists validated-add-songs-ops)
         (concat validated-add-playlist-ops)
         (update-and-write output-filename known-entities))))
