(ns highspot-takehome-exercise.io
  (:require [cheshire.core :as ch]))

(defn write-as-json
  [filename content]
  (->> content
      ch/generate-string
      (spit filename)))

(defn read-entire-json-file-into-memory
  "note: reads entire file into memory
  should use java.io reader and some kind of lazy/stream parsing for bigger files"
  [filename]
  (-> filename
      slurp
      (ch/parse-string true)))
